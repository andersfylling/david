//
// Created by anders on 5/10/17.
//

#include "david/EngineContext.h"

/**
 * Constructor
 */
david::EngineContext::EngineContext()
{}

/**
 * Destructor
 */
david::EngineContext::~EngineContext()
{
  /// Does not have ownership!
//  if (this->searchPtr != nullptr) {
//    delete this->searchPtr;
//  }
//
//  if (this->neuralNetworkPtr != nullptr) {
//    delete this->neuralNetworkPtr;
//  }
//
//  if (this->gameTreePtr != nullptr) {
//    delete this->gameTreePtr;
//  }
//
//  if (this->uciProtocolPtr != nullptr) {
//    delete this->uciProtocolPtr;
//  }
}
